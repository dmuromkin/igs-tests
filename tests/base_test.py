#!/usr/local/bin/python
# coding: utf-8
import os
import time
import unittest

from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By


class BaseTest(unittest.TestCase):

    def test_base_controls(self):
        os.chdir("..")
        path = os.path.abspath(os.curdir)+"\chromedriver"
        options = Options()
        options.add_argument("--disable-extensions")
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("--headless")
        options.add_argument("--disable-gpu");
        self.driver = webdriver.Chrome(executable_path=path, options=options)
        self.driver.get("http://igs1.test.ruitb.ru/")
        time.sleep(3)
        password_field = self.driver.find_element(By.XPATH, "//span[contains(@class, 'input')]//input")
        password_field.send_keys("admin")
        password_field.send_keys(Keys.ENTER)
        time.sleep(2)
        menu_links = self.driver.find_element(By.XPATH, "//div[@class='app-header']").find_elements(By.XPATH, './/li')
        expected_links = [' Карта', ' Оперативная обстановка', ' Все сообщения']
        # Проверка наличия всех разделов меню и их названий
        for i in menu_links:
            text = i.find_element(By.XPATH, './/span//a').get_attribute('innerHTML')
            self.assertEqual(text, expected_links[expected_links.index(text)])

        try:
            self.driver.find_element(By.XPATH, "//div[@class='app-header-right']").find_elements(By.TAG_NAME, 'button')[0]
            self.driver.find_element(By.XPATH, "//div[@class='app-header-right']").find_elements(By.TAG_NAME, 'button')[1]
            self.driver.find_element(By.XPATH, "//div[@class='app-header-right']").find_elements(By.TAG_NAME, 'button')[2]
            self.driver.find_element(By.XPATH, "//div[@class='app-header-right']").find_elements(By.TAG_NAME, 'button')[3]
            self.driver.find_element(By.XPATH, "//div[@class='map-ol-map']")
            self.driver.find_element(By.XPATH, "//div[@id='map-user-control-mi-bm']//button//img")
            self.driver.find_element(By.XPATH, "//div[@id='map-user-control-mi-dimensions']//button//img")
            self.driver.find_element(By.XPATH, "//div[@id='map-user-control-mi-print']//button//img")
        except NoSuchElementException:
            print("Проблемы с отображением карты / инструментов карты ")
            raise NoSuchElementException

        # Переход на ОО
        # menu_links[1].click()
        # time.sleep(2)
        #
        # try:
        #     self.driver.find_element(By.XPATH, "//div[contains(@class,'stopCollapsible stopsForward')]")
        #     self.driver.find_element(By.XPATH, "//div[contains(@class,'stopCollapsible stopsBackward')]")
        #     self.driver.find_element(By.XPATH, "//div[@class='routeSituation']")
        #     self.driver.find_element(By.XPATH, "//div[@class='routeSituationHeader']")
        #     self.driver.find_element(By.XPATH, "//div[@class='routeSituationBox']")
        #     plotContainers = self.driver.find_elements(By.XPATH, "//div[@class='plotContainer']")
        #     self.assertEqual(len(plotContainers), 2)
        # except NoSuchElementException:
        #     print("Проблемы с отображением элементов ОО ")
        #     raise NoSuchElementException
        # except AssertionError:
        #     print("Неверное кол-во графиков")
        #     raise AssertionError

        # Переход на события
        # menu_links[2].click()
        # time.sleep(1)
        #
        # try:
        #     self.driver.find_element(By.XPATH, "//div[contains(@class,'group-critial')]")
        #     self.driver.find_element(By.XPATH, "//div[contains(@class,'group-warn')]")
        #     self.driver.find_element(By.XPATH, "//div[contains(@class,'group-info')]")
        # except NoSuchElementException:
        #     print("Проблемы с отображением элементов раздела События ")
        #     raise NoSuchElementException
